//
// Created by Mateusz on 30.03.2018.
//

#include "useful_functions.h"

std::random_device rd; // NOLINT
double pi() { return std::atan(1) * 4; }

double unirandi(float start, float end) {
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(start, std::nextafter(end, DBL_MAX));;

    return dist(mt);
}

float Umap(float value, float istart, float istop, float ostart, float ostop){
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

sf::Color setColor(int hue, float sat, float val) {
    hue %= 360;
    while (hue < 0) hue += 360;

    if (sat < 0.f) sat = 0.f;
    if (sat > 1.f) sat = 1.f;

    if (val < 0.f) val = 0.f;
    if (val > 1.f) val = 1.f;

    int h = hue / 60;
    float f = float(hue) / 60 - h;
    float p = val*(1.f - sat);
    float q = val*(1.f - sat*f);
    float t = val*(1.f - sat*(1 - f));

    switch (h)
    {
        default:
        case 0:
        case 6: return {static_cast<Uint8>(val * 255), static_cast<Uint8>(t * 255),
                                 static_cast<Uint8>(p * 255)};
        case 1: return {static_cast<Uint8>(q * 255), static_cast<Uint8>(val * 255),
                                 static_cast<Uint8>(p * 255)};
        case 2: return {static_cast<Uint8>(p * 255), static_cast<Uint8>(val * 255),
                                 static_cast<Uint8>(t * 255)};
        case 3: return {static_cast<Uint8>(p * 255), static_cast<Uint8>(q * 255),
                                 static_cast<Uint8>(val * 255)};
        case 4: return {static_cast<Uint8>(t * 255), static_cast<Uint8>(p * 255),
                                 static_cast<Uint8>(val * 255)};
        case 5: return {static_cast<Uint8>(val * 255), static_cast<Uint8>(p * 255),
                                 static_cast<Uint8>(q * 255)};
    }
}
