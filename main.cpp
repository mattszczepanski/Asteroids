#include <SFML/Graphics.hpp>
#include <iostream>
#include "Asteroid.h"

using namespace sf;

constexpr int windowWidth{800}, windowHeight{600};
constexpr int sizeOfAsteroid{245};

struct Debree {
    CircleShape shape;
    Vector2f position, velocity, acceleration;

    Debree(float dx, float dy, float debree_radius) {
        position = {dx, dy};
        shape.setFillColor(Color::White);
        shape.setRadius(debree_radius);
        shape.setOrigin(debree_radius, debree_radius);
        shape.setPosition(position);
    }

    void attach_to_ball(float x, float y){
        shape.setPosition(x+position.x,y);
    }
};

struct Ball {
    CircleShape shape;
    Vector2f position, velocity, acceleration;
    int ball_radius = 55;
    vector<Debree> debrees;
    int cooldown{0};

    Ball(float x, float y) {
        position = {x, y};
        shape.setRadius(ball_radius);
        shape.setFillColor(setColor(51, 51, 51));
        shape.setPosition(position);
        shape.setOrigin(ball_radius, ball_radius);
    }

    void fill_ball(float mposX, float mposY) {
        if (cooldown == 0 && (abs(mposX - position.x) < ball_radius and abs(mposY - position.y) < ball_radius)) {
            float radius{static_cast<float>(unirandi(3, 15))};

            debrees.emplace_back(mposX, mposY, radius);

            cooldown = 5;
        } else if (cooldown > 0) {
            cooldown--;
        };}

    void show_debrees(RenderWindow &target) {
        for (int i{0}; i < debrees.size(); i++) {
            target.draw(debrees.at(static_cast<unsigned int>(i)).shape);
        }
    }

    Vector2f apply_mouse_force(float pox, float poy) {
        Vector2f mouse_force{pox, poy};
        return mouse_force;
    }

    void iterate_debrees(){
        for (auto &debree : debrees) debree.attach_to_ball(position.x, position.y);
    }
    void update(RenderWindow &window) {
        iterate_debrees();
        float mouse_posX, mouse_posY;
        mouse_posX = Mouse::getPosition(window).x;
        mouse_posY = Mouse::getPosition(window).y;
        if (Mouse::isButtonPressed(Mouse::Button::Left)) {
            fill_ball(mouse_posX, mouse_posY);
//            acceleration = apply_mouse_force(mouse_posX/10000000, mouse_posY/10000000);
//            std::cout<<"mouse pos: X: "<<Mouse::getPosition(window).x<<" "<<Mouse::getPosition(window).y<<std::endl;
        }

        velocity += acceleration;
        position += velocity;

        shape.setPosition(position);

        acceleration.x = 0;
        acceleration.y = 0;
    }
};

int main() {
    srand(static_cast<unsigned int>(time(nullptr)));
//    Asteroid asteroid_test(windowWidth / 2.f, windowHeight / 2.f, sizeOfAsteroid);
    // create the window
    Ball ball(windowWidth / 2, windowHeight / 2);
    RenderWindow window(VideoMode(windowWidth, windowHeight), "Lighting");

    // run the program as long as the window is open
    while (window.isOpen()) {

        Event event;
        while (window.pollEvent(event)) {
            // "close requested" event: we close the window
            if (event.type == Event::Closed)
                window.close();
        }
        if (Keyboard::isKeyPressed(Keyboard::Key::Escape)) break;


        // clear the window with black color
        window.clear(Color::Black);
        window.draw(ball.shape);
        ball.update(window);
//        for (auto &debree : ball.debrees) {
//            window.draw(debree.shape);
//        }
        ball.show_debrees(window);
//        asteroid_test.update(windowWidth, windowHeight);
//        asteroid_test.show(window);
        window.display();
    }

    return 0;
}