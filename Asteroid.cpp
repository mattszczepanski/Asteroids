//
// Created by Mateusz on 30.03.2018.
//

#include "Asteroid.h"

void Asteroid::show(RenderWindow &window) {
    window.draw(shape);
}

void Asteroid::update(int windowWidth, int windowHeight) {
    edges(windowWidth, windowHeight);
    sf::Transform M;
    position += velocity;
    M.translate(position);
    for (int i{0}; i < points.size(); i++) {
        shape[i].position = M.transformPoint(points[i]);
    }
}

Asteroid::Asteroid(float aX, float aY, int radius) {
    size_of_asteroid = static_cast<int>(unirandi(20,25));
//    velocity = {static_cast<float>(unirandi(0, 0.5)), static_cast<float>(unirandi(0, 0.5))};
    position = {aX, aY};
    r = radius;
    off = r / 4;
    shape.setPrimitiveType(sf::LinesStrip);
    shape.resize(static_cast<size_t>(size_of_asteroid + 1));
    for (int j{0}; j < size_of_asteroid; j++) {
        offsets.push_back(static_cast<float>(unirandi(-off, off)));
    }
    for (int i{0}; i < size_of_asteroid; i++) {
        float angle = Umap(i, 0, size_of_asteroid, 0, 2 * (float) pi());
        float rof = r + offsets.at((unsigned int) (i));
        float x = rof * cos(angle);
        float y = rof * sin(angle);
        points.emplace_back(Vector2f(x, y));
    }
    points.emplace_back(points.at(0));
}

void Asteroid::edges(int windowWidth, int windowHeight) {
    if (x() < -r) { position.x = windowWidth + r; }
    if (x() > windowWidth + r) { position.x = -r; }
    if (y() < 0 - r) { position.y = windowHeight + r; }
    if (y() > windowHeight + r) { position.y = -r; }
}

float Asteroid::x() {
    return position.y;
}

float Asteroid::y() {
    return position.y;
}

