//
// Created by Mateusz on 30.03.2018.
//
#pragma once
#include <SFML/Graphics.hpp>
#include "useful_functions.h"

using namespace sf;
using  namespace std;


class Asteroid {
public:
    int size_of_asteroid;
    Vector2f position, velocity;
    VertexArray shape;
    vector<Vector2f> points;
    vector<float> offsets;
    int r;
    float off;
    Asteroid(float aX, float aY, int radius);
    void update(int windowWidth, int windowHeight);
    void show(RenderWindow &window);
    void edges(int windowWidth, int windowHeight);
    float x();

    float y();

private:
};

